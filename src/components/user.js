import React, { Component } from 'react'
import {userinfo} from '../action/userinfo'
import { connect} from 'react-redux'
import './../App.css'
class User extends Component {
    
    componentWillMount(){
        const {id} =this.props.match.params
        this.props.dispatch(userinfo(id)) 
        // console.log("lllllllllllllllllllllllllll",this.props)  
    }

    render() {
        // const userId=this.props.id

        console.log("userinfo props",this.props)
        return (
            <div className="user_container">
                <img className="avatar" src={this.props.user.avatar}/>
                <p>{this.props.user.first_name} {this.props.user.last_name}</p>
            </div>
        )
    }
}
const mapStateToProps = state =>({
    user:  state.users.user

})
export default connect( mapStateToProps )(User)
