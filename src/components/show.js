import React, { Component } from 'react'
import { connect} from 'react-redux'
import { fetchPosts } from '../action/postaction'
import { Route , BrowserRouter as Router ,Link } from 'react-router-dom'

class Show extends Component {
    componentWillMount(){
        this.props.dispatch(fetchPosts())
        console.log("eeeeeeeeeeeeeee")
    }
    
    render() {
        // console.log("this.props",this.props)
        
        return(
            <div>   
                {
                this.props.users.map(user =>{
                    return  (
                                <div key={user.id}>
                                    <Link to={`/user/${user.id}`} >{user.first_name} {user.last_name}</Link>
                                </div>
                            )
                    }
                    )  
                }
                 
            </div>
        )
    }
}
const mapStateToProps = state =>({
    users:state.users.users

})
// const mapDispatchToProps = state =>({
// })
export default connect(mapStateToProps)(Show)