import React from 'react';
import './App.css';
import {Provider} from 'react-redux'
import store from './reducers /store'
import Show from './components/show'
import User from './components/user'

import {  BrowserRouter as Router , Route ,Switch } from 'react-router-dom'
function App() {
  // console.log("dfsadf")
  return (
    
    
    <Provider store={store}>
          <Router>
              <Switch>    

                <Route path="/" exact component={Show}  />
                <Route path="/user/:id" component={User} />
                
              </Switch>
        </Router>
          </Provider>
    
  );
}

export default App;
