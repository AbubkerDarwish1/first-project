
const initialstate={
    users: [],
    user: {},
}
export const users = (state = initialstate , action) => {

    switch (action.type) {
        case 'FETCH_USERS':
            
            return { ...state ,
                users: action.payload.data
            }
            case 'FETCH_USER_INFO':
                return { ...state ,
                    user: action.payload.data
                }
        default:
            return state
    }

}
