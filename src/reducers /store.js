import {createStore , applyMiddleware } from 'redux'
import logger from "redux-logger"
import thunk from 'redux-thunk'
// import {fetchPosts} from '../action/postaction'
import combineReducers from '../reducers/combine'


const middleware =[thunk , logger]
// const initialstate ={ users:[], userinfo:{} } 
 const store = createStore( combineReducers , applyMiddleware(...middleware))

//  store.dispatch(thunk(fetchPosts))
export default store



